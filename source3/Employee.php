<?php
namespace vitalikadmin\oop\source3;

//Задача 10.1: Сделайте класс Employee, в котором будут следующие свойства: name (имя), surname (фамилия) и salary (зарплата).
//Задача 10.2: Сделайте так, чтобы свойства name и surname были доступны только для чтения, а свойство salary - и для чтения, и для записи.


class Employee
{
    private $name = 'Habib';
    private $surname = 'Golopupkin';
    private $salary;

    private function setName($name) {
        $this->name = $name;
    }
    public function getName() {
        return $this->name;
    }


    private function setSurname($surname) {
        $this->surname = $surname;
    }
    public function getSurname() {
        return $this->surname;
    }


    public function setSalary($salary) {
        $this->salary = $salary;
    }

    public function getSalary() {
        return $this->salary;
    }
}