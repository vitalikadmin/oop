<?php
require_once 'vendor/autoload.php';


function pre() {
    echo '<pre>';
}
echo 'TASK 1 ======================================================================================================= >';
pre();

$user = new \vitalikadmin\oop\source\Employee();

$user->name = "Ivan";
$user->age = 25;
$user->salary = 1000;

$user1 = new \vitalikadmin\oop\source\Employee();

$user1->name = 'Вася';
$user1->age = 26;
$user1->salary = 2000;

echo "Сумма зарплаты работника $user->name и работника $user1->name = ";
echo $user->salary + $user1->salary;

pre();


echo "Сумма возрастов работников $user->name и $user1->name = ";

echo $user->age + $user1->age;

pre();
echo 'TASK 2 =======================================================================================================> ';
pre();


$user2 = new \vitalikadmin\oop\source\Employee();
$user3 = new \vitalikadmin\oop\source\Employee();
echo $user->getName();

echo $user->checkAge();

$user2->name = 'Nikolay';
$user2->age = 50;
$user2->salary = 20000;

$user3->name = 'Viktor';
$user3->age = 30;
$user3->salary = 10000;
pre();


echo "Сума зарплаты работников $user2->name and $user3->name = ";
echo $user2->getSalary() + $user3->getSalary();

pre();
echo 'TASK 3 ======================================================================================================= >';
pre();


$user4 = new \vitalikadmin\oop\source\User();

$user4->name = 'Коля';
$user4->age = 25;

$user4->setAge(30);
echo $user4->age;

pre();
echo 'TASK 4 =======================================================================================================> ';
pre();


$employeer = new \vitalikadmin\oop\source2\Employee();

$employeer->salary = 5000;

echo $employeer->doubleSalary();

pre();
echo 'TASK 5 =======================================================================================================> ';
pre();


$rectangle = new \vitalikadmin\oop\source\Rectangnle();

echo $rectangle->getSquare();

pre();
echo $rectangle->getPerimeter();

pre();
echo 'TASK 6 ========================================================================================================>';
pre();


$user5 = new \vitalikadmin\oop\sorce2\User();

$user5->age = 30;

echo $user5->setAge(50);
pre();
echo $user5->addAge(50,2);

pre();
echo $user5->subAge(50, 1);
pre();
echo $user5->validationAge(50);


pre();
echo 'TASK 8 ========================================================================================================>';
pre();

$student = new \vitalikadmin\oop\source\Student();
$adminname = new \vitalikadmin\oop\source\Student();
$student->course = 1;

echo $student->transferToNextCourse(5);

//echo $student->courseAdministrator = 'Nikolya'; //Вызовет ошибку иби свойсво приватное

$adminname->setCourseAdministrator('Vitalik');
pre();
echo $adminname->getCourseAdministrator();


pre();
echo 'TASK 9 ========================================================================================================>';
pre();

$employeer = new \vitalikadmin\oop\sorce2\Employee();

$employeer->setName('Romansiy');
echo $employeer->getName();

pre();
$employeer->setAge(12);
$employeer->setAge(60);
echo $employeer->getAge();

pre();
$employeer->setSalary(1000);
echo $employeer->getSalary();
pre();

echo 'TASK 10 =======================================================================================================>';
pre();

$Piple = new \vitalikadmin\oop\source3\Employee();

//$Piple->setName('HHH'); //private method wiil error :-)
echo $Piple->getName();
pre();
//$Piple->setSurname('Pupkin');//private method wiil error :-)
echo $Piple->getSurname();

pre();

$Piple->setSalary(2000);
echo $Piple->getSalary();

pre();

echo 'TASK 11 =======================================================================================================>';

pre();

$piple1 = new vitalikadmin\oop\source4\Employee('Вася', 25, 1000);
$piple2 = new \vitalikadmin\oop\source4\Employee('Путя', 30, 2000);

echo $piple1->salary + $piple2->salary;

pre();
echo '====================================================END=========================================================';