<?php
namespace vitalikadmin\oop\source4;

//Задача 11.1: Сделайте класс Employee, в котором будут следующие публичные свойства - name (имя), age (возраст), salary (зарплата).
//Сделайте так, чтобы эти свойства заполнялись в методе __construct при создании объекта.
//Задача 11.2: Создайте объект класса Employee с именем 'Вася', возрастом 25, зарплатой 1000.
//Задача 11.3: Создайте объект класса Employee с именем 'Петя', возрастом 30, зарплатой 2000.
//Задача 11.4: Выведите на экран сумму зарплат Васи и Пети.

class Employee
{
    public $name;
    public $age;
    public $salary;

    public function __construct($name, $age, $salary)
    {
        $this->name = $name;
        $this->age = $age;
        $this->salary = $salary;
    }

}

//class User
//{
//    public $name;
//    public $age;
//
//    // Конструктор объекта:
//    public function __construct($name, $age)
//    {
//        $this->name = $name; // можно задать значения по умолчанию
//        $this->age = $age;
//    }
//}
//$user = new User('Коля', 25);
//echo $user->name; // выведет 'Коля'
//echo $user->age; // выведет 25
//$prop = 'name';
//echo $user->$prop; // выведет 'Коля'