<?php


namespace vitalikadmin\oop\source;
//Задача 8.1: Сделайте класс Student со свойствами $name и $course (курс студента, от 1-го до 5-го).
//Задача 8.2: В классе Student сделайте public метод transferToNextCourse, который будет переводить студента на следующий курс.
//Задача 8.3: Выполните в методе transferToNextCourse проверку на то, что следующий курс не больше 5.
//Задача 8.4: Вынесите проверку курса в отдельный private метод isCourseCorrect.
//Задача 8.5: Сделайте в классе Student приватное свойство $courseAdministrator.
//Задача 8.6: Попробуйте записать в это свойство любое имя снаружи класса. Убедитесь, что это будет вызывать ошибку.
//Задача 8.7: Чтобы была возможность записать это имя, создайте публичный метод (называется сеттер) setCourseAdministrator,
// который будет принимать аргументом имя администратора и записывать его в приватное свойство  $courseAdministrator.
//Задача 8.8: Чтобы была возможность прочитать это имя, создайте публичный метод (называется геттер) getCourseAdministrator
//, который будет возвращать приватное свойство  $courseAdministrator.


class Student
{
    public $name;
    public $course;
    private $courseAdministrator;

    public function transferToNextCourse($nextCourse) {
        $this->isCourseCorrect($nextCourse);
            return $this->course = $nextCourse;
    }

    private function  isCourseCorrect($nextCourse) {
        if($nextCourse > 5) {
            echo "Curse is not correct";
        }
    }

    public function setCourseAdministrator($adminname) {
        $this->courseAdministrator = $adminname;
    }

    public function getCourseAdministrator() {

        return $this->courseAdministrator;

    }
}