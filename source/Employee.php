<?php


namespace vitalikadmin\oop\source;

//дача 1.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
//Задача 1.2: Создайте объект класса Employee, затем установите его свойства в следующие значения - имя 'Иван', возраст 25, зарплата 1000.
//Задача 1.3: Создайте второй объект класса Employee, установите его свойства в следующие значения - имя 'Вася', возраст 26, зарплата 2000.
//Задача 1.4: Выведите на экран сумму зарплат Ивана и Васи.
//Задача 1.5: Выведите на экран сумму возрастов Ивана и Васи.

class Employee
{
    public $name;
    public $age;
    public $salary;
//constructor (val1, val2) {
//    this.val1 = val1 || 5;
//    this,val2 = val2 || 10;
//}
// user = new Employee();
////    public function getInfo() {
////        echo "Name is " . $this->name . ", age = " . $this->age . ", salary = " . $this->salary;
//
//}

    public function getName() {
        return $this->name;
    }

    public function getAge() {
        return $this->age;
    }

    public function getSalary() {
        return $this->salary;
    }

    public function checkAge() {
//        return $this->age > 18 ? "true" : "false";

        if ($this->age > 18) {
            return 'TRUE';
        }
         return false;
    }
}

//Задача 2.1: Сделайте класс Employee (работник), в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата).
//Задача 2.2: Сделайте в классе Employee метод getName, который будет возвращать имя работника.
//Задача 2.3: Сделайте в классе Employee метод getAge, который будет возвращать возраст работника.
//Задача 2.4: Сделайте в классе Employee метод getSalary, который будет возвращать зарплату работника.
//Задача 2.5: Сделайте в классе Employee метод checkAge, который будет проверять то, что работнику больше 18 лет и возвращать true, если это так, и false, если это не так.
//Задача 2.6: Создайте два объекта класса Employee, запишите в их свойства какие-либо значения. С помощью метода getSalary найдите сумму зарплат созданных работников.